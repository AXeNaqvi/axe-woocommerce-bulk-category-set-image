<?php
/**
 * Plugin Name: AXe WooCommerce Bulk Category Set Image
 * Plugin URI: https://bitbucket.org/AXeNaqvi/axe-woocommerce-bulk-category-set-image
 * Description: Adds support to WooCommerce product categories to set a single image for multiple selected categories.
 * Author: AXe
 * Author URI: https://bitbucket.org/AXeNaqvi
 * Version: 1.0.0
 * WC requires at least: 3.7.1
 * WC tested up to: 3.7.1
 * Copyright: (c) 2019 AXe (axe.naqvi@gmail.com)
 *
 * License: GNU General Public License v2.0 and above
 * License URI: http://www.gnu.org/licenses/gpl-2.0.html
 *
 */

if ( ! defined( 'ABSPATH' )) exit;

define('AXEWOOBULKCATIMAGE','axe-woocommerce-bulk-category-set-image');

class AXe_WBSCI{
	function __construct(){
		add_action('after_setup_theme',array($this, 'axe_wbcsi_init'),9999);
	}

	function axe_wbcsi_init(){
		add_action('admin_footer', array($this,'axe_product_cat_bulk_assign_image'),100);
	}

	function axe_product_cat_bulk_assign_image(){
		$screen = get_current_screen();
		$taxonomy = isset($screen->taxonomy) ? $screen->taxonomy : '';
		$base = isset($screen->base) ? $screen->base : '';
		if($taxonomy == 'product_cat' && $base == 'edit-tags'){
			$this->loadCSS();
			$this->loadForm();
			$this->loadScript();
			$this->processSubmission();
		}
	}
	private function loadCSS(){
		$style = array();
		$style['#wpfooter'] = 'padding-bottom:50px;';
		$style['#axe_catimageform'] = 'background:#333;position:fixed;width:100%;bottom:0px;left:0px;z-index:999999;box-sizing:border-box';
		$style['#axe_catimageform.axe_goback_catimageform'] = 'z-index:0;';
		$style['#axe_catimageform *'] = 'box-sizing:border-box';
		$style['#axe_catimageform ol'] = 'margin:0px 15px;';
		$style['#axe_catimageform ol li'] = 'list-style-type:decimal';
		$style['#axe_catimageform #axe_catimageform_container'] = 'position:relative;';
		$style['#axe_catimageform #axe_catimageform_innercontainer'] = 'position:relative;display:none;padding:10px 10px 20px';
		$style['#axe_catimageform #axe_catimage_intro'] = 'font-size:11px;color:#FFF';
		$style['#axe_catimageform #axe_catimage_intro .axe_catimageform_title'] = 'font-size:14px;';
		$style['#axe_catimageform #axe_catimageform_container a#axe_form_slider'] = 'position:absolute; top:-30px; right:20px; width:30px; height:30px; background:#333; padding:5px; line-height:15px; font-size:30px; text-align:center; font-weight:700; text-decoration:none; color:#FFF;';
		$style['#axe_catimageform #axe_catimageform_container a#axe_form_slider.withanimation'] = 'animation: axe_bounce 6s infinite linear;';
		$style['#axe_catimageform #axe_catimageformdiv'] = '';
		$style['#axe_catimageform input'] = 'width:24%;margin:0px 0.5%;display:inline-block;';
		$style['#axe_catimageform input.axe_ciform_button'] = 'cursor:pointer';
		$style['#axe_catimageform .axe_cif_formelement'] = 'height:27px;font-size:12px;border:1px solid #FFF';
		$style['#axe_catimageform .axe_cif_smallfont'] = 'font-size:11px;';
		$style['a#axe_cat_get_ids'] = 'display:inline-block;width:24%;margin:0px 0.5%;color:#FFF;text-align:center;border:1px solid #FFF;text-decoration:none;padding:3px 10px;';
		$styling = '';
		foreach($style as $key => $val){
			$styling .= $key . '{' . trim($val,'{}') . '}';
		}
		$animations = '
		@keyframes axe_rotation {
			0%, 40% { transform: rotate(0deg); }
			60%, 100% { transform: rotate(90deg); }
		}
		@keyframes axe_bounce {
			0%, 80% { top: -30px; }
			90%, 92% { top: -40px; }
			92%, 94% { top: -30px; }
			94%, 95% { top: -35px; }
			95%, 96% { top: -30px; }
			96%, 97% { top: -32px; }
			97%, 98% { top: -30px; }
			98%, 99% { top: -31px; }
			99%, 100% { top: -30px; }
		}';
		$styling .= $animations;
		echo '<style>'.$styling.'</style>';
	}
	private function loadForm(){
		$catidplaceholder = __('Select category IDs from above list and click the Get Selected IDs button.', AXEWOOBULKCATIMAGE);
		$catimageplaceholder = __('Click here to select an image from media.', AXEWOOBULKCATIMAGE);
		$getidsanchor = __('Get Selected Ids', AXEWOOBULKCATIMAGE);
		$confirmation = __('Are you sure you want to assign the image to all selected categories?', AXEWOOBULKCATIMAGE);
		$submitbutton = __('Set Image', AXEWOOBULKCATIMAGE);
		$formheading = __('AXe WooCommerce Bulk Category Set Image', AXEWOOBULKCATIMAGE);
		$formtoggler = __('Click here to toggle form.', AXEWOOBULKCATIMAGE);
		$instruction_bold = __('Instructions: ', AXEWOOBULKCATIMAGE);
		$instructions = __('Check all the boxes above that you want the image to set and then click \'Get Selected IDs\' button. Set the image id in the corresponding field and press \'Set Image\' button.', AXEWOOBULKCATIMAGE);
		$instructionset = '';
		$instructions = explode('.', trim($instructions,'.'));
		if($this->isValidArray($instructions)){
			$instructionset = '<strong><span class="axe_catimageform_title">'.$formheading . '</span><br/>' . $instruction_bold.'</strong>';
			$instructionset .= '<ol><li>' . implode('</li><li>', $instructions). '</li></ol>';
		}

		?>
		<div id="axe_catimageform"><div id="axe_catimageform_container"><a href="#" id="axe_form_slider" title="<?php echo $formheading . ': ' . $formtoggler; ?>" class="withanimation">+</a><div id="axe_catimageform_innercontainer"><div id="axe_catimage_intro"><?php echo $instructionset; ?></div><div id="axe_catimageformdiv"><form action="" method="post"><input type="text" id="axe_catids" class="axe_cif_smallfont axe_cif_formelement" name="axe_catids" value="" title="<?php echo $catidplaceholder; ?>" placeholder="<?php echo $catidplaceholder; ?>" readonly="readonly" required="required"/><input type="text" id="axe_cat_img_id" class="axe_ciform_button axe_cif_smallfont axe_cif_formelement" name="axe_cat_img_id" placeholder="<?php echo $catimageplaceholder; ?>" title="<?php echo $catimageplaceholder; ?>" required="required"/><a id="axe_cat_get_ids" class="axe_ciform_button axe_cif_formelement" href="#" title="<?php echo $getidsanchor; ?>"><?php echo $getidsanchor; ?></a><input type="hidden" name="redirectto" id="axe_cat_redirectto" name="axe_cat_redirectto" value="" /><input type="submit" class="axe_ciform_button axe_cif_formelement" title="<?php echo $submitbutton; ?>" onclick="return confirm('<?php echo $confirmation; ?>');" value="<?php echo $submitbutton; ?>"/></form></div></div></div></div>
		<?php
	}
	private function loadScript(){
		?>
		<script>
			(function($){
				$(document).ready(function(){
					axePCCFormSlider();
					axeSetPCCIdsFormAction();
					axeGetPCCIdsHook();
					axeSetPCCImageId();
					function axePCCFormSlider(){
						$('#axe_catimageform #axe_catimageform_container a#axe_form_slider').on('click',function(e){
							e.preventDefault();
							e.stopPropagation();
							var t = $(this),
							tt = t.text() == '+' ? '-' : '+';
							if(t.hasClass('withanimation')){
								t.removeClass('withanimation');
							}
							t.text(tt);
							$('#axe_catimageform #axe_catimageform_innercontainer').slideToggle();
						});
					}
					function axeSetPCCIdsFormAction(){
						var act = window.location.href;
						$('#catimageform form').attr('action', act);
						$('#axe_cat_redirectto').val(encodeURI(act));
					}
					function axeGetPCCIdsHook(){
						$('a#axe_cat_get_ids').on('click',function(e){
							e.preventDefault();
							e.stopPropagation();
							axeGetPCCIds();
						});
					}
					function axeGetPCCIds(){
						if($('.check-column').length){
							var axe_pcc = $('.check-column'),
							tar = $('#axe_catids'),
							baseval = tar.val(),
							axe_catids_val = [];

							axe_pcc.each(function(){
								axe_pcc_child = $('input[type="checkbox"]:checked',$(this));
								axe_pcc_val = parseInt(axe_pcc_child.val());
								if(axe_pcc_val) axe_catids_val.push(axe_pcc_val);
							});

							tar.val(axe_catids_val.join(','));
						}
					}
					function axeSetPCCImageId(){
						var file_frame;
						var wp_media_post_id = wp.media.model.settings.post.id; /*// Store the old id*/
						var set_to_post_id = 10; /*// Set this*/
						var axet = null;
						var cform = $('#axe_catimageform');

						$('#axe_cat_img_id').live('click', function( event ){
							axet =$(this);
							cform.addClass('axe_goback_catimageform');

							event.preventDefault();

							/*If the media frame already exists, reopen it.*/
							if ( file_frame ) {
								/*Set the post ID to what we want*/
								file_frame.uploader.uploader.param( 'post_id', set_to_post_id );
								/*Open frame*/
								file_frame.open();
								return;
							} else {
								/*Set the wp.media post id so the uploader grabs the ID we want when initialised*/
								wp.media.model.settings.post.id = set_to_post_id;
							}

							/*Create the media frame.*/
							file_frame = wp.media.frames.file_frame = wp.media({
								title: $( this ).data( 'uploader_title' ),
								button: {
									text: $( this ).data( 'uploader_button_text' ),
								},
								multiple: false  /*// Set to true to allow multiple files to be selected*/
							});

							file_frame.on('close',function() {
								cform.removeClass('axe_goback_catimageform');
							});

							/*When an image is selected, run a callback.*/
							file_frame.on( 'select', function() {
								/*We set multiple to false so only get one image from the uploader*/
								attachment = file_frame.state().get('selection').first().toJSON();

								cform.removeClass('axe_goback_catimageform');
								/*Do something with attachment.id and/or attachment.url here*/
								axet.val(attachment.id);

								/*Restore the main post ID*/
								wp.media.model.settings.post.id = wp_media_post_id;

							});

							/*Finally, open the modal*/
							file_frame.open();
						});

						/*Restore the main ID when the add media button is pressed*/
						$('a.add_media').on('click', function() {
							/*axet = jQuery(this);*/
							wp.media.model.settings.post.id = wp_media_post_id;
						});
					}
				});
			})(jQuery);
		</script>
		<?php
	}

	private function processSubmission(){
		if(isset($_POST['axe_catids']) && isset($_POST['axe_cat_img_id'])){
			$axe_cat_img_id = absint($_POST['axe_cat_img_id']);
			$axe_cat_redirectto = isset($_POST['axe_cat_redirectto']) ? urldecode($_POST['axe_cat_redirectto']) : '';
			$axe_catids = array_map('absint', explode(',', $_POST['axe_catids']));
			if($axe_cat_img_id && $this->isValidArray($axe_catids)){
				foreach($axe_catids as $axecatid){
					update_term_meta( $axecatid, 'thumbnail_id', $axe_cat_img_id );
				}
				echo '<script>window.location = "'.$axe_cat_redirectto.'";</script>';
			}
		}
	}

	private function isValidArray($arr){
		$res = false;
		if(is_array($arr)){
			if(sizeof($arr)){
				$res = true;
			}
		}
		return $res;
	}
}
if (in_array('woocommerce/woocommerce.php', apply_filters('active_plugins', get_option('active_plugins')))) {
	new AXe_WBSCI();
}