#AXe WooCommerce Bulk Category Set Image

**Plugin URI:** [https://bitbucket.org/AXeNaqvi/axe-woocommerce-bulk-category-set-image/](https://bitbucket.org/AXeNaqvi/axe-woocommerce-bulk-category-set-image/)

**Contributors:** axenaqvi

**Author:** AZ Naqvi

**Author URI:** [https://bitbucket.org/AXeNaqvi/](https://bitbucket.org/AXeNaqvi/)

**Version:** 1.0.0

**Stable tag:** 1.0.0

**Requires at least:** Wordpress 4.7

**Tested up to:** Wordpress 5.2.4

**WC requires at least:** 3.7.1

**WC tested up to:** 3.7.1

**Text Domain:** axe-woocommerce-bulk-category-set-image

**License:** GNU General Public License v2.0 and above

**License URI:** http://www.gnu.org/licenses/gpl-2.0.html

**Description:** A simple plugin for setting a single image to multiple selected categories in WooCommerce.


##Description

This plugin adds support to WooCommerce Category section to set a single image to multiple selected categories.

##Changelog:
---
###Ver 1.0.0
1. Tested upto WP 5.2.4
2. Tested upto WC 3.7.1
3. Adds support to set single image to multiple selected categories.